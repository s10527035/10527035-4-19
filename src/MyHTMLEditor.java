import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.scene.web.HTMLEditor;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import javafx.scene.layout.HBox;

import java.awt.*;

public class MyHTMLEditor extends Application{
    public static void main(String[] args)
    {
        launch(args);
    }

    @Override
        public void start(Stage  primaryStage) throws Exception
        {
            VBox vBox=new VBox();
            vBox.setPadding(new Insets(2,2,2,2));
            vBox.setSpacing(2);
            HTMLEditor htmlEditor=new HTMLEditor();
            htmlEditor.setStyle("-fx-border-style: dotted");
            htmlEditor.setPrefHeight(300);
            Button btnSource =new Button("Get Source");


            TextArea htmlCode=new TextArea();
            htmlCode.setEditable(false);
            btnSource.setOnAction(e->{
                htmlCode.setText(htmlEditor.getHtmlText());
            });
            htmlCode.setPrefHeight();
            htmlCode.setWrapText(true);

            WebView webView=new WebView();
            WebEngine webEngine=webView.getEngine();

            btnView.setOnAction(e->{
                webEngine.loadContent(htmlEditor.getHtmlText());
             });



            vBox.getChildren().addAll(htmlEditor,btnview, btnSource, htmlCode);
            Scene scene=new Scene(new Group(),600,400);
            scene.setRoot(vBox);
            primaryStage.setTitle("My First HTML Editor");
            primaryStage.setScene(scene);
            setUserAgentStylesheet(STYLESHEET_MODENA);
            primaryStage.show();
        }
    }

